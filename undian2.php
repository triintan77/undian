<!DOCTYPE html>
<html>
<head>
  <title>undian 2</title>
  <style>
    body{
  letter-spacing: 9px;
  font-size: 60px;
  text-align:center;
  line-height:100vh;
  font-family: Courier;
}

html{
  background: #000;
  color: #fff;
}

button {
    background: transparent;
    color: #fff;
    border: 1px solid;
    padding: 10px 20px;
    font-size: 20px;
    border-radius: 2px;
    cursor: pointer;
}
button:hover {
    background: #fff;
    color:#000;
    transition: all 0.5s ease-in-out;
}
  </style>
</head>
<body>
<button onClick="start()">Start</button>
<button onclick="back()">Back</button>
</body>

<script>
  let names = [
"1111111111  Tri",
"2222222222  Intan",
"3333333333  Ismayanty",
"4444444444  Elfara",
"5555555555  Nadiva",
"6666666666  Putri",
"7777777777  Raden",
"8888888888  Azfa",
"9999999999  Faris",
"1212121212  Adillah",
"2121212121  Putra",
"1313131313  Emir",
"3131313131  Othman",
"1414141414  Jordan",
"4141414141  Bandu",
"1515151515  Ridwan",
"5151515151  Mutamasiqin",
"1616161616  Azriel",
"6161616161  Maulana",
"1717171717  Boni",
"7171717171  Bam",
"1818181818  Annie",
"8181818181  Irene",
"1919191919  Chelsea",
"9191919191  Rahma",
"1234567890  Miima",
"1987654321  Tia"
];

  count = names.length;

function start(){
  selected = names[Math.random() * count | 0].toUpperCase();
  covered = selected.replace(/[^\s]/g, '_');
  document.body.innerHTML = covered;
  timer = setInterval(decode, 10);
}

function decode(){
  newtext = covered.split('').map(changeLetter()).join('');
  document.body.innerHTML = newtext;
  if(selected == covered){
    clearTimeout(timer);
    winnerRevealed();
    return false;
  }
  covered = newtext;
}

function changeLetter(){
  replacements = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz%!@&*#_ ';
  replacementsLen = replacements.length;
  return function(letter, index, err){
    return selected[index] == letter 
       ? letter 
       : replacements[Math.random() * replacementsLen | 0];
  }
}

function winnerRevealed(){
  alert('winner found');
}
</script>
</html>